import React from 'react';

import HomePageBanner from "../components/HomePage/HomePageBanner";
import Layout from "@/src/components/Layouts/Layout";

function HomePageView() {
    return (
        <div>
            <Layout>
                <HomePageBanner/>
            </Layout>
        </div>
    );
}

export default HomePageView;
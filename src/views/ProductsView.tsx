import React from 'react';
import Layout from "../components/Layouts/Layout";


function ProductsView() {
    return (
        <div>
            <Layout>
                Products
            </Layout>
        </div>
    );
}

export default ProductsView;
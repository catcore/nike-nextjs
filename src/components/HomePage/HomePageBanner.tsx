import React from 'react';
import Image from 'next/image';
import Link from "next/link";

function HomePageBanner() {
    return (
        <section
            className='w-full h-[550px] relative'
        >
            <Image
                src={'/Images/nike.jpg'}
                alt={'Bannière'}
                fill
                className='object-cover'
            />

            <div
            className='absolute top-0 left-0 w-full h-full text-white p-14 flex flex-col
            space-y-14 justify-end'
            >
                <div
                    className='flex flex-col space-y-5'
                >
                    <h2
                    className={`font-medium capitalize`}
                    >
                        {`Nike Style By`}
                    </h2>
                    <h1
                    className={`text-7xl font-extrabold uppercase`}
                    >
                        {`NIKE V2K RUN`}
                    </h1>
                </div>
                <div
                    className='flex flex-col space-y-8 items-start'
                >
                    <p>
                        {`Découvre ce nouveau modèle avec une vibe rétro`}
                    </p>
                    <Link
                    href={'/products'}
                    className={`bg-white text-black px-3 py-2 text-medium rounded-full
                    hover:bg-[#757575]`}
                    >
                        {`Acheter`}
                    </Link>
                </div>
            </div>

        </section>
    );
}

export default HomePageBanner;
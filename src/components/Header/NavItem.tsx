import React from 'react';
import Link from "next/link";

function NavItem({linkRef, text}: {
    linkRef: string,
    text: string
}) {
    return (
        <li
            className='pl-3 font-medium text-base'
        >
            <Link
                href={linkRef}
                className='hover:text-[#111111] hover:underline'
            >
                <span
                    className={'font-medium '}
                >
                    {text}
                </span>
            </Link>

        </li>
    );
}

export default NavItem;
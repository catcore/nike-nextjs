import React from 'react';
import Link from "next/link";

function AboutNavItem({linkRef, text}: {
    linkRef: string,
    text: string
}) {
    return (
        <li
            className='pl-3 text-xs font-medium'
        >
            <Link
                href={linkRef}
                className='hover:text-[#757575] hover:underline'
            >
                <span>
                    {text}
                </span>
            </Link>

        </li>
    );
}

export default AboutNavItem;
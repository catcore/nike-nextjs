import React from 'react';
import Link from "next/link";
import AboutNavItem from "@/src/components/AboutNav/AboutNavItem";

function AboutNav() {
    return (
        <div
            className='w-full px-12 py-4 bg-black/5 flex justify-between items-center'>
            <div
                className='flex flex-row space-x-2'>
                <div className='w-7 h-7 bg-black rounded-full'>

                </div>
                <div className='w-7 h-7 bg-black rounded-full'>

                </div>
            </div>

            <nav
                className=''>
                <ul
                    className='flex space-x-3 divide-x-2 divide-black items-center'
                >
                    <AboutNavItem linkRef={'/'} text={`Trouver un magasin`}/>
                    <AboutNavItem linkRef={'/'} text={`Aide`}/>
                    <AboutNavItem linkRef={'/'} text={`Nous rejoindre`}/>
                    <AboutNavItem linkRef={'/'} text={`S'identifier`}/>
                </ul>
            </nav>
        </div>
    );
}
export default AboutNav;
import React from 'react';
import NavItem from "../Header/NavItem";
import {MagnifyingGlassIcon} from "@heroicons/react/16/solid";
import {HeartIcon, ShoppingBagIcon} from "@heroicons/react/24/outline";


function Header() {
    return (
        <div
            className='w-full h-16 px-12 py-4 bg-white flex justify-between'
        >
            <div
                className='w-1/3'
            >
                <div
                    className='w-7 h-7 bg-black rounded-full'
                >
                </div>
            </div>
            <div>
                <ul
                    className='flex'
                >
                    <NavItem linkRef={'/'} text={'Nouveautés'}/>
                    <NavItem linkRef={'/'} text={'Homme'}/>
                    <NavItem linkRef={'/'} text={'Femme'}/>
                    <NavItem linkRef={'/'} text={'Enfant'}/>
                    <NavItem linkRef={'/'} text={'Offres'}/>
                </ul>
            </div>
            <div
                className='flex gap-2 items-center w-1/3 justify-end'
            >
                <div
                    className='w-48 h-10 bg-black/5 rounded-full flex items-center pl-2.5 gap-2'
                >
                    <div
                        className='w-7 h-7 rounded-full'
                    >
                        <MagnifyingGlassIcon
                            className='w-7'
                        ></MagnifyingGlassIcon>
                    </div>
                    <span>Rechercher</span>
                </div>
                <div
                    className='w-7 h-7 rounded-full'
                >
                    <HeartIcon
                        className='icon-button'
                    ></HeartIcon>
                </div>
                <div
                    className='w-7 h-7 rounded-full'
                >
                    <ShoppingBagIcon
                        className='icon-button'
                    ></ShoppingBagIcon>
                </div>
            </div>
        </div>
    );
}

export default Header;
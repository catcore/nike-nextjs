import React from 'react';
import Link from "next/link";

function Footer() {
    return (
        <div>
            <div
            className={`flex`}
            >
                <div
                    className={`w-2/3 bg-black text-white px-12 py-12 space-x-6 flex justify-between`}
                >
                    <ul
                        className={`space-y-4`}
                    >
                        <li>
                            <Link
                                href={'/'}
                                className={`font-bold text-[14px]`}
                            >
                                CARTES CADEAUX
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-bold text-[14px]`}
                            >
                                TROUVER UN MAGASIN
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-bold text-[14px]`}
                            >
                                NIKE JOURNAL
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-bold text-[14px]`}
                            >
                                DEVENIR MEMBRE
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-bold text-[14px]`}
                            >
                                REDUCTION POUR ETUDIANTS
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-bold text-[14px]`}
                            >
                                COMMENTAIRES
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-bold text-[14px]`}
                            >
                                CODES PROMO
                            </Link>
                        </li>
                    </ul>
                    <ul
                        className={`space-y-2`}
                    >
                        <li>
                            <Link
                                href={'/'}
                                className={`font-bold text-[14px]`}
                            >
                                AIDE
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                                Statut de commande
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                                Expédition et livraison
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                                Retours
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                                Mode de paiement
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                                Nous contacter
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                                Aide - Codes promo Nike
                            </Link>
                        </li>
                    </ul>
                    <ul
                        className={`space-y-2`}
                    >
                        <li>
                            <Link
                                href={'/'}
                                className={`font-bold text-[14px]`}
                            >
                                A PROPOS DE NIKE
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                                Actualités
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                                Carrières
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                                Investisseurs
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                                Développement durable
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                                Mission
                            </Link>
                        </li>
                    </ul>
                    <ul
                        className={`space-y-2`}
                    >
                        <li>
                            <Link
                                href={'/'}
                                className={`font-bold text-[14px]`}
                            >
                                REJOINS-NOUS
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                              Nike App
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                             Nike Run Club
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                                Nike Training Club
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                                SNKRS
                            </Link>
                        </li>
                    </ul>
                </div>
                <div
                    className={`w-1/3 bg-black px-12 py-12 space-x-6 flex justify-end`}
                >
                    <div
                        className='w-7 h-7 bg-white rounded-full'
                    >
                    </div>
                    <div
                        className='w-7 h-7 bg-white rounded-full'
                    >
                    </div>
                    <div
                        className='w-7 h-7 bg-white rounded-full'
                    >
                    </div>
                    <div
                        className='w-7 h-7 bg-white rounded-full'
                    >
                    </div>
                </div>
            </div>
            <div
            className={`flex`}
            >
                <div
                    className={`w-1/2 bg-black px-12 py-12 flex items-end`}
                >
                    <div
                        className={`flex text-white space-x-6`}
                    >
                        <div
                            className='w-7 h-7 bg-white rounded-full'
                        >
                        </div>
                        <span>
                            FRANCE
                        </span>
                        <span
                        className={`text-[#757575]`}
                        >
                            © 2023 Nike, Inc. Tous droits réservés
                        </span>
                    </div>
                </div>
                <div
                className={`w-1/2 bg-black px-12 py-12`}
                >
                    <ul
                        className={`flex flex-wrap gap-x-6 gap-y-2`}
                    >
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                                Guides
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                                {`Conditions d'utilisation`}
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                                Conditions générales de vente
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                                Mentions légales
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                                Politique en matière de confidentialité des cookies
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={'/'}
                                className={`font-medium text-[12px] text-[#757575]`}
                            >
                                Paramètres des cookies
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    );
}

export default Footer;